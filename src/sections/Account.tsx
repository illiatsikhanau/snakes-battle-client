import {useAppDispatch} from '../hooks';
import {setSnakeColor, setUsername} from '../slice';
import {useAppSelector} from '../hooks';
import {getCookie, setCookie, removeCookie} from 'typescript-cookie';
import {Buffer} from 'buffer';
import axios from '../axios';
import Tabs from '../components/tabs/Tabs';
import TabList from '../components/tabs/TabList';
import Tab from '../components/tabs/Tab';
import TabPanels from '../components/tabs/TabPanels';
import TabPanel from '../components/tabs/TabPanel';
import FormSubmit from '../components/form/FormSubmit';
import FormInput from '../components/form/FormInput';
import Form from '../components/form/Form';
import FormAlert from '../components/form/FormAlert';
import TabSection from '../components/tabs/TabSection';
import FormSection from '../components/form/FormSection';
import Text from '../components/Text';
import FormColorPicker from '../components/form/FormColorPicker';

const Account = () => {
  const dispatch = useAppDispatch();
  const username = useAppSelector((state) => state.app.username);
  const snakeColor = useAppSelector((state) => state.app.snakeColor);

  const changeSnakeColor = async (
    fields: Map<string, {value: string, error: string}>,
    setSubmitMessage: (message: string, color: string) => void
  ) => {
    try {
      const newColor = fields.get('New color')?.value;
      await axios.patch('/my/update', {snake_color: newColor});
      dispatch(setSnakeColor(newColor ?? ''));
      setSubmitMessage('Snake color has been successfully updated', 'green');
    } catch {
      setSubmitMessage('Service unavailable, try again later', 'red');
    }
  };

  const changeUsername = async (
    fields: Map<string, {value: string, error: string}>,
    setSubmitMessage: (message: string, color: string) => void
  ) => {
    const newUsername = fields.get('Username')?.value.trim() as string;
    const password = fields.get('Current password')?.value.trim() as string;
    const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64');

    if (token === getCookie('token')) {
      try {
        await axios.post('/check_username', {username: newUsername});
        await axios.patch('/my/update', {username: newUsername});
        dispatch(setUsername(newUsername));
        setCookie('token', Buffer.from(`${newUsername}:${password}`, 'utf8').toString('base64'));
        setSubmitMessage('Username has been successfully changed', 'green');
      } catch {
        setSubmitMessage('Username is taken', 'red');
      }
    } else {
      setSubmitMessage('Invalid password', 'red');
    }
  };

  const changePassword = async (
    fields: Map<string, {value: string, error: string}>,
    setSubmitMessage: (message: string, color: string) => void
  ) => {
    const newPassword = fields.get('New password')?.value.trim() as string;
    const newPassword2 = fields.get('Password confirmation')?.value.trim() as string;
    const currentPassword = fields.get('Current password')?.value.trim() as string;

    if (newPassword === newPassword2) {
      const currentToken = getCookie('token');
      try {
        setCookie('token', Buffer.from(`${username}:${currentPassword}`, 'utf8').toString('base64'));
        await axios.patch('/my/update', {password: newPassword});
        logOut();
      } catch {
        setCookie('token', currentToken);
        setSubmitMessage('Invalid current password', 'red');
      }
    } else {
      setSubmitMessage('New passwords do not match', 'red');
    }
  };

  const logOut = () => {
    removeCookie('token');
    dispatch(setUsername(''));
  };

  return (
    <Tabs>
      <TabList>
        <Tab>Account</Tab>
      </TabList>
      <TabPanels>
        <TabPanel>
          <TabSection>
            <Form action={changeSnakeColor} undecorated>
              <FormAlert />
              <FormSection direction='row'>
                <FormSection direction='col'>
                  <Text size='normal' nowrap align='left'>
                    Snake appearance
                  </Text>
                </FormSection>
                <FormSection direction='col'>
                  <FormSection direction='row'>
                    <Text size='small' nowrap align='left' opacity='normal'>
                      Current color
                    </Text>
                    <FormColorPicker initialValue={snakeColor} disabled />
                  </FormSection>
                  <FormSection direction='row'>
                    <Text size='small' nowrap align='left' opacity='normal'>
                      New color
                    </Text>
                    <FormColorPicker name='New color' initialValue={snakeColor} />
                  </FormSection>
                  <FormSubmit
                    text='Update color'
                    size='small'
                    color='teal'
                  />
                </FormSection>
              </FormSection>
            </Form>
          </TabSection>
          <TabSection>
            <Form action={changeUsername} undecorated>
              <FormAlert />
              <FormSection direction='row'>
                <FormSection direction='col'>
                  <Text size='normal' nowrap align='left'>
                    Change username
                  </Text>
                </FormSection>
                <FormSection direction='col'>
                  <FormInput
                    type='text'
                    name='Username'
                    size='small'
                    initialValue={username}
                    minLength={3}
                    maxLength={32}
                  />
                  <FormInput
                    type='password'
                    name='Current password'
                    size='small'
                    minLength={3}
                    maxLength={32}
                  />
                  <FormSubmit
                    text='Update username'
                    size='small'
                    color='teal'
                  />
                </FormSection>
              </FormSection>
            </Form>
          </TabSection>
          <TabSection>
            <Form action={changePassword} undecorated>
              <FormAlert />
              <FormSection direction='row'>
                <FormSection direction='col'>
                  <Text size='normal' nowrap align='left'>
                    Change password
                  </Text>
                  <Text size='small' color='gray' align='left'>
                    After a successful password update,
                    you will be redirected to the login page
                    where you can log in with your new password.
                  </Text>
                </FormSection>
                <FormSection direction='col'>
                  <FormInput
                    type='password'
                    name='New password'
                    size='small'
                    minLength={3}
                    maxLength={32}
                  />
                  <FormInput
                    type='password'
                    name='Password confirmation'
                    size='small'
                    minLength={3}
                    maxLength={32}
                  />
                  <FormInput
                    type='password'
                    name='Current password'
                    size='small'
                    minLength={3}
                    maxLength={32}
                  />
                  <FormSubmit
                    text='Update password'
                    size='small'
                    color='teal'
                  />
                </FormSection>
              </FormSection>
            </Form>
          </TabSection>
          <TabSection>
            <FormSubmit
              action={logOut}
              text='Log Out'
              size='small'
              color='red'
              short
              outline
            />
          </TabSection>
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
}

export default Account;

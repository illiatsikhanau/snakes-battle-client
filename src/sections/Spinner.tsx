import './Spinner.scss';

const Spinner = () => {
  return (
    <div className='spinner'>
      <span className='loader'/>
    </div>
  );
}

export default Spinner;

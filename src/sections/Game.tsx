import './Game.scss';
import {useState, useMemo, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import {useAppSelector} from '../hooks';
import {socket, sendMessage} from '../socket';
import Text from '../components/Text';
import FormSubmit from '../components/form/FormSubmit';
import Form from '../components/form/Form';
import FormSection from '../components/form/FormSection';

enum Direction {UP, RIGHT, DOWN, LEFT}
interface Pos {x: number, y: number}
enum PosType {FREE, SNAKE, FOOD, ENEMY}

const arenaSize = 12;
const generateArena = (snake: Pos[] = [], food: Pos[] = [], enemy: Pos[] = []): PosType[][] => {
  const arena = Array.from({length: arenaSize},() =>
    Array.from({length: arenaSize}, () => PosType.FREE)
  );
  snake.filter(p => p.x >= 0 && p.y >= 0).forEach(p => arena[p.y][p.x] = PosType.SNAKE);
  food.filter(p => p.x >= 0 && p.y >= 0).forEach(p => arena[p.y][p.x] = PosType.FOOD);
  enemy.filter(p => p.x >= 0 && p.y >= 0).forEach(p => arena[p.y][p.x] = PosType.ENEMY);
  return arena;
};

const changeDirection = (e: KeyboardEvent) => {
  let direction = -1;
  switch (e.code) {
    case 'ArrowUp':
    case 'KeyW':
      direction = Direction.UP;
      break;
    case 'ArrowLeft':
    case 'KeyA':
      direction = Direction.LEFT;
      break;
    case 'ArrowDown':
    case 'KeyS':
      direction = Direction.DOWN;
      break;
    case 'ArrowRight':
    case 'KeyD':
      direction = Direction.RIGHT;
      break;
  }
  sendDirection(direction);
};

const sendDirection = (direction: number) => {
  if (direction >= 0) {
    sendMessage({
      action: 'input',
      direction: direction
    });
  }
};

const Game = () => {
  const navigate = useNavigate();
  const username = useAppSelector((state) => state.app.username);
  const enemyUsername = useAppSelector((state) => state.app.enemyUsername);
  const snakeColor = useAppSelector((state) => state.app.snakeColor);
  const enemySnakeColor = useAppSelector((state) => state.app.enemySnakeColor);
  const [arena, setArena] = useState(generateArena());
  const [winner, setWinner] = useState('');
  const cells = useMemo(() => arena.map(row => row.map(cell => {
    switch (cell) {
      case PosType.SNAKE:
        return snakeColor;
      case PosType.FOOD:
        return '#718096';
      case PosType.ENEMY:
        return enemySnakeColor;
      default:
        return '';
    }
  })), [arena, snakeColor, enemySnakeColor]);
  const [gameOver, setGameOver] = useState(false);

  useEffect(() => {
    if (socket) {
      socket.onmessage = ({data}) => {
        const message = JSON.parse(data);
        switch (message.action) {
          case 'game-over':
            setWinner(message.winner);
            setGameOver(true);
            break;
          case 'arena-update':
            setArena(generateArena(message.snake, message.food, message.enemy));
            break;
        }
      };
      document.removeEventListener('keydown', e => changeDirection(e));
      document.addEventListener('keydown', e => changeDirection(e));
    }
  }, []);

  useEffect(() => () => {
    sendMessage({action: 'game-over'});
    document.removeEventListener('keydown', e => changeDirection(e));
  }, []);

  const restart = () => {
    setGameOver(false);
  };

  const back = () => {
    navigate('/games');
  };

  const controller = (
    <div className='controller'>
      <div className='key'/>
      <div className='key active up' onClick={() => sendDirection(Direction.UP)}>
        <Text size='big'>W</Text>
      </div>
      <div className='key'/>
      <div className='key active left' onClick={() => sendDirection(Direction.LEFT)}>
        <Text size='big'>A</Text>
      </div>
      <div className='key'/>
      <div className='key active right' onClick={() => sendDirection(Direction.RIGHT)}>
        <Text size='big'>D</Text>
      </div>
      <div className='key'/>
      <div className='key active down' onClick={() => sendDirection(Direction.DOWN)}>
        <Text size='big'>S</Text>
      </div>
      <div className='key'/>
    </div>
  );

  return (
    <div className='game'>
      <div className='controller-container hidden'>
        {controller}
      </div>
      <div className='view'>
        {enemyUsername &&
          <FormSection direction='row'>
            <Text size='big' align='right' style={{color: snakeColor}}>{username}</Text>
            <Text size='big' color='gray'>vs</Text>
            <Text size='big' align='left' style={{color: enemySnakeColor}}>{enemyUsername}</Text>
          </FormSection>
        }
        <div className='arena'>
          {cells.map((row, rowIndex) =>
            <div className='row' key={rowIndex}>
              {row.map((cell, colIndex) =>
                <div className='cell' key={colIndex} style={{background: cell}}/>
              )}
            </div>
          )}
        </div>
        {gameOver &&
          <Form action={restart} undecorated direction='row'>
            <Text size='big' color='gray'>Game over</Text>
            <FormSubmit text='Ok' color='teal' size='small' action={back}/>
            <Text size='big' color='gray'>Winner: {winner}</Text>
          </Form>
        }
      </div>
      <div className='controller-container'>
        {controller}
      </div>
    </div>
  );
}

export default Game;

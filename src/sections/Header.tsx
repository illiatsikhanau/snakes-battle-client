import './Header.scss';
import {Link} from 'react-router-dom';
import {useAppSelector, useAppDispatch} from '../hooks';
import {setTheme} from '../slice';
import {getCookie} from 'typescript-cookie';
import axios from '../axios';
import logo from '../assets/logo.svg';
import moon from '../assets/moon.svg';
import sun from '../assets/sun.svg';
import user from '../assets/user.svg';
import Icon from '../components/Icon';

const Header = () => {
  const theme = useAppSelector((state) => state.app.theme);
  const dispatch = useAppDispatch();

  const changeTheme = async () => {
    const newTheme = theme === 'light' ? 'dark' : 'light';
    dispatch(setTheme(newTheme));
    try {
      getCookie('token') && await axios.patch('/my/update', {theme: newTheme});
    } catch {}
  };

  return (
    <header className='header'>
      <Link to='/'>
        <Icon src={logo} size='big' color='teal'/>
      </Link>
      <div className='header-menu'>
        <Icon
          src={theme === 'light' ? moon : sun}
          size='big'
          onClick={changeTheme}
          hoverable
          color='teal'
        />
        <Link to='/account'>
          <Icon src={user} size='big' color='teal'/>
        </Link>
      </div>
    </header>
  );
}

export default Header;

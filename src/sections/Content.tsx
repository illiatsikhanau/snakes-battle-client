import './Content.scss';
import {ChildrenProps} from '../props/ChildrenProps';

const Content = (props: ChildrenProps) => {
  return (
    <section className='content'>
      {props.children}
    </section>
  );
}

export default Content;

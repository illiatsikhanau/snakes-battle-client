import {useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {useAppDispatch} from '../hooks';
import {setEnemySnakeColor, setEnemyUsername} from '../slice';
import {sendMessage, socket} from '../socket';
import Text from '../components/Text';
import Form from '../components/form/Form';
import FormSubmit from '../components/form/FormSubmit';
import Tab from '../components/tabs/Tab';
import TabList from '../components/tabs/TabList';
import TabPanel from '../components/tabs/TabPanel';
import TabPanels from '../components/tabs/TabPanels';
import TabSection from '../components/tabs/TabSection';
import Tabs from '../components/tabs/Tabs';
import FormSection from '../components/form/FormSection';

const Games = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [matching, setMatching] = useState(false);

  useEffect(() => {
    if (socket) {
      socket.onmessage = ({data}) => {
        const message = JSON.parse(data);
        switch (message.action) {
          case 'multi-player-start':
            dispatch(setEnemyUsername(message.enemyUsername));
            dispatch(setEnemySnakeColor(message.enemySnakeColor));
            navigate('/game');
            break;
        }
      };
    }
  }, [dispatch, navigate]);

  const singlePlayerGame = () => {
    dispatch(setEnemyUsername(''));
    navigate('/game');
    sendMessage({action: 'single-player-start'});
  };

  const changeSearching = () => {
    const newMatching = !matching;
    if (newMatching) {
      sendMessage({action: 'matching-start'});
    }
    setMatching(newMatching);
  };

  return (
    <Tabs>
      <TabList>
        <Tab>Single-player</Tab>
        <Tab>Multi-player</Tab>
      </TabList>
      <TabPanels>
        <TabPanel>
          <TabSection>
            <Form action={singlePlayerGame} undecorated direction='row'>
              <Text size='small' color='gray' nowrap align='left'>
                Improve your skills
              </Text>
              <FormSubmit
                text='New game'
                size='small'
                color='teal'
              />
            </Form>
          </TabSection>
        </TabPanel>
        <TabPanel>
          <TabSection>
            <Form undecorated direction='col' action={changeSearching}>
              <FormSection direction='row'>
                <Text size='small' color='gray' nowrap align='left'>
                  Challenge other players
                </Text>
                <FormSubmit
                  text={matching ? 'Cancel matching' : 'Request matching'}
                  color='teal'
                  size='small'
                />
              </FormSection>
            </Form>
          </TabSection>
          {matching &&
            <TabSection>
              <Text size='small' color='gray'>Matching...</Text>
            </TabSection>
          }
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
}

export default Games;

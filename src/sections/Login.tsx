import './Login.scss';
import logo from '../assets/logo.svg';
import {getCookie, setCookie} from 'typescript-cookie';
import {Buffer} from 'buffer';
import axios from '../axios';
import {useAppDispatch, useAppSelector} from '../hooks';
import {setUsername, setRoles, setTheme, setSnakeColor} from '../slice';
import {setSocket} from '../socket';
import {Link, useNavigate, useLocation} from 'react-router-dom';
import Form from '../components/form/Form';
import FormInput from '../components/form/FormInput';
import FormSubmit from '../components/form/FormSubmit';
import FormAlert from '../components/form/FormAlert';
import Text from '../components/Text';
import Icon from '../components/Icon';
import FormSection from '../components/form/FormSection';

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const theme = useAppSelector((state) => state.app.theme);

  const location = useLocation();
  const isLoginType = location.pathname === '/login';

  const action = (
    fields: Map<string, {value: string, error: string}>,
    setSubmitMessage: (message: string, color: string) => void
  ) => {
    const username = fields.get('Username')?.value.trim() as string;
    const password = fields.get('Password')?.value.trim() as string;
    const password2 = fields.get('Confirm password')?.value.trim() as string;
    isLoginType ?
      logIn(username, password, setSubmitMessage) :
      signUp(username, password, password2, setSubmitMessage);
  };

  const logIn = async (
    username: string,
    password: string,
    setSubmitMessage: (message: string, color: string) => void
  ) => {
    setCookie('token', Buffer.from(`${username}:${password}`, 'utf8').toString('base64'));
    try {
      const info = await axios.get('/my/info');
      updateAppInfo(info.data);
      navigate('/games');
    } catch {
      setSubmitMessage('Invalid login or password', 'red');
    }
  };

  const signUp = async (
    username: string,
    password: string,
    password2: string,
    setSubmitMessage: (message: string, color: string) => void
  ) => {
    if (password === password2) {
      try {
        await axios.post('/check_username', {username: username});
        await axios.post('/signup', {
          username: username,
          password: password,
          theme: theme,
        });
        await logIn(username, password, setSubmitMessage);
      } catch {
        setSubmitMessage('Username is taken', 'red');
      }
    } else {
      setSubmitMessage('Passwords do not match', 'red');
    }
  };

  const updateAppInfo = (data: any) => {
    dispatch(setUsername(data.username));
    data.roles && dispatch(setRoles(data.roles));
    data.theme && dispatch(setTheme(data.theme));
    data.snake_color && dispatch(setSnakeColor(data.snake_color));
    setSocket(new WebSocket(`${process.env.REACT_APP_WS_ADDRESS}?token=${getCookie('token')}`));
  };

  return (
    <div className='login'>
      <Form action={action}>
        <FormSection direction='row'>
          <Icon src={logo} size='big' color='teal'/>
        </FormSection>
        <FormAlert/>
        <FormInput
          type='text'
          name='Username'
          size='normal'
          minLength={3}
          maxLength={32}
        />
        <FormInput
          type='password'
          name='Password'
          size='normal'
          minLength={3}
          maxLength={32}
        />
        {!isLoginType &&
          <FormInput
            type='password'
            name='Confirm password'
            size='normal'
            minLength={3}
            maxLength={32}
          />
        }
        <FormSubmit
          text={isLoginType ? 'Log In' : 'Sign Up'}
          size='normal'
          color='teal'
        />
        <Text size='small' color='gray'>
          {isLoginType ? `Don't have an account?` : `Already have an account?`}
          <Link to={isLoginType ? '/signup' : '/login'}>{isLoginType ? 'Sign Up' : 'Log In'}</Link>
        </Text>
      </Form>
    </div>
  );
}

export default Login;

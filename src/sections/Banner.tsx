import './Banner.scss';
import {useNavigate} from 'react-router-dom';
import FormSubmit from '../components/form/FormSubmit';
import Text from '../components/Text';
import Form from '../components/form/Form';

const Banner = () => {
  const navigate = useNavigate();

  return (
    <div className='banner'>
      <Text size='biggest' weight='bold' color='gradient'>
        Play with your friends in time-tested amazing game!
      </Text>
      <Text size='big' opacity='normal' color='gray'>
        Challenge your friends and try to win.
        Start as a small worm and try to get bigger by eating.
        How long can you survive in this world?
      </Text>
      <Form undecorated direction='row'>
        <FormSubmit
          action={() => navigate('/signup')}
          text='Sign Up'
          size='big'
          color='teal'
        />
        <FormSubmit
          action={() => navigate('/login')}
          text='Log In'
          size='big'
          color='teal'
        />
      </Form>
    </div>
  );
}

export default Banner;

import './Footer.scss';
import gitlab from '../assets/gitlab.svg';
import {Link} from 'react-router-dom';
import Text from '../components/Text';
import Icon from '../components/Icon';

const Footer = () => {
  return (
    <footer className='footer'>
      <Text size='small' color='gray'>Made by Ilya Tsikhanau</Text>
      <Link to='https://gitlab.com/illiatsikhanau'>
        <Icon src={gitlab} size='normal' color='gray'/>
      </Link>
    </footer>
  );
}

export default Footer;

let socket: WebSocket | null;

const setSocket = (ws: WebSocket) => {
  if (socket) socket.close();
  socket = ws;
};

const sendMessage = (message: object) => {
  if (socket && socket.readyState === socket.OPEN) socket.send(JSON.stringify(message));
};

export {socket, setSocket, sendMessage}

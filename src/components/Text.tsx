import './Text.scss';
import {ChildrenProps} from '../props/ChildrenProps';
import {ColorProps} from '../props/ColorProps';
import {SizeProps} from '../props/SizeProps';
import {AlignProps} from '../props/AlignProps';
import {getClassName} from '../helper';

interface TextProps extends ChildrenProps, SizeProps, ColorProps, AlignProps {
  weight?: string
  opacity?: string
  nowrap?: boolean
  style?: object
}

const Text = (props: TextProps) => {
  const className = getClassName('text', props, [
    'size', 'weight', 'opacity', 'color', 'nowrap', 'align'
  ]);

  return (
    <p className={className} style={props.style}>
      {props.children}
    </p>
  );
}

export default Text;

import './Icon.scss';
import {SizeProps} from '../props/SizeProps';
import {ColorProps} from '../props/ColorProps';
import {getClassName} from '../helper';

interface ImageProps extends SizeProps, ColorProps {
  src: string,
  onClick?: () => void
  hoverable?: boolean
}

const Icon = (props: ImageProps) => {
  const className = getClassName('icon', props, ['size', 'color', 'hoverable']);

  return (
    <div className={className} onClick={props.onClick}>
      <img className='image' src={props.src} alt=''/>
    </div>
  );
}

export default Icon;

import './TabSection.scss';
import {TabsProps} from './TabsProps';

const TabSection = (props: TabsProps) => {
  return (
    <section className='tab-section'>
      {props.children}
    </section>
  );
}

export default TabSection;

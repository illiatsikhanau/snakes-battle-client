import {ReactElement} from 'react';

export interface TabsProps {
  children: ReactElement | ReactElement[]
  selected?: number
  setSelected?: (index: number) => void
}

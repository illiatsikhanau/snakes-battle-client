import './TabPanel.scss';
import {ChildrenProps} from '../../props/ChildrenProps';

const TabPanel = (props: ChildrenProps) => {
  return (
    <div className='tab-panel'>
      {props.children}
    </div>
  );
}

export default TabPanel;

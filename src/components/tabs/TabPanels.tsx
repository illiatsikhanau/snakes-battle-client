import './TabPanels.scss';
import {TabsProps} from './TabsProps';
import {Children} from 'react';

const TabPanels = (props: TabsProps) => {
  return (
    <div className='tab-panels'>
      {Children.map(props.children, (child, index) =>
        index === props.selected &&
        <div className='tab-panel-content'>
          {child}
        </div>
      )}
    </div>
  );
}

export default TabPanels;

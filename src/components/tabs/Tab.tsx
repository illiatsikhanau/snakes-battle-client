import {ChildrenProps} from '../../props/ChildrenProps';
import Text from '../Text';

const Tab = (props: ChildrenProps) => {
  return (
    <Text size='normal' weight='bold'>
      {props.children}
    </Text>
  );
}

export default Tab;

import './Tabs.scss';
import {TabsProps} from './TabsProps';
import {useState, Children, cloneElement} from 'react';

const Tabs = (props: TabsProps) => {
  const [selected, setSelected] = useState(0);

  return (
    <div className='tabs'>
      {Children.map(props.children, child =>
        cloneElement(child, {
          selected: selected,
          setSelected: setSelected
        })
      )}
    </div>
  );
}

export default Tabs;

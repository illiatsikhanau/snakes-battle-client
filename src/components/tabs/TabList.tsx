import './TabList.scss';
import {TabsProps} from './TabsProps';
import {Children} from 'react';

const TabList = (props: TabsProps) => {
  const onTabClick = (index: number) => {
    props.setSelected && props.setSelected(index);
  };

  return (
    <div className='tab-list'>
      {Children.map(props.children, (child, index) =>
        <div
          className={`content ${index === props.selected ? 'selected' : ''}`}
          onClick={() => onTabClick(index)}
        >
          {child}
        </div>
      )}
    </div>
  );
}

export default TabList;

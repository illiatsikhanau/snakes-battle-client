import './FormSubmit.scss';
import React from 'react';
import {FormProps} from './FormProps';
import {SizeProps} from '../../props/SizeProps';
import {ColorProps} from '../../props/ColorProps';
import {getClassName} from '../../helper';

interface FormSubmitProps extends FormProps, SizeProps, ColorProps {
  action?: () => void
  text?: string
  short?: boolean
  outline?: boolean
}

const FormSubmit = (props: FormSubmitProps) => {
  const onClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    if (props.action) {
      props.action();
    } else if (props.submit && props.fields) {
      props.submit();
    }
  }

  const className = getClassName('form-submit', props, [
    'size', 'color', 'short', 'outline'
  ]);

  return (
    <button className={className} onClick={onClick}>
      {props.text}
    </button>
  );
}

export default FormSubmit;

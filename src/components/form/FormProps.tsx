import {ReactElement} from 'react';

export interface FormProps {
  children?: false | ReactElement | (false | ReactElement)[]
  name?: string
  fields?: Map<string, {value: string, error: string}>
  submit?: () => void
  submitMessage?: string
  submitMessageColor?: string
  setSubmitMessage?: (message: string, color: string) => void
}

import './FormInput.scss';
import React, {useEffect, useState} from 'react';
import {FormProps} from './FormProps';
import {SizeProps} from '../../props/SizeProps';
import {ColorProps} from '../../props/ColorProps';
import Text from '../Text';
import {getClassName} from '../../helper';

interface FormInputProps extends FormProps, SizeProps, ColorProps {
  type: string
  autoComplete?: string
  initialValue?: string
  onValueChange?: (value: string) => void
  minLength?: number
  maxLength?: number
}

const FormInput = (props: FormInputProps) => {
  const [value, setValue] = useState(props.initialValue ?? '');

  useEffect(() => {
    if (props.name) {
      let error = '';
      if (props.minLength && value.trim().length < props.minLength) {
        error = `The '${props.name}' shall be at least ${props.minLength} symbols`;
      }

      props.fields?.set(props.name, {
        value: value,
        error: error,
      });
    }
  }, [props, value]);

  useEffect(() => () => {
    props.fields?.delete(props.name ?? '');
  }, [props]);

  const onValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = e.target.value;
    if (props.maxLength) {
      value = value.substring(0, props.maxLength);
    }

    props.onValueChange && props.onValueChange(value);
    setValue(value);
  };

  const className = getClassName('field', props, ['size']);

  return (
    <div className='form-input'>
      <Text size={props.size} color={props.color} align='left' opacity='normal'>
        {props.name}
      </Text>
      <input
        className={className}
        type={props.type}
        autoComplete={props.autoComplete}
        name={props.name}
        value={value}
        onChange={onValueChange}
      />
    </div>
  );
}

export default FormInput;

import './FormSection.scss';
import {FormProps} from './FormProps';
import {AlignProps} from '../../props/AlignProps';
import {DirectionProps} from '../../props/DirectionProps';
import {Children, cloneElement} from 'react';
import {getClassName} from '../../helper';

interface SectionProps extends FormProps, AlignProps, DirectionProps {
}

const FormSection = (props: SectionProps) => {
  const className = getClassName('form-section', props, ['direction', 'align']);

  return (
    <div className={className}>
      {Children.map(props.children, child =>
        child ?
          cloneElement(child, {
            fields: props.fields,
            submit: props.submit,
            submitMessage: props.submitMessage,
            submitMessageColor: props.submitMessageColor,
            setSubmitMessage: props.setSubmitMessage
          }) :
          child
      )}
    </div>
  );
}

export default FormSection;

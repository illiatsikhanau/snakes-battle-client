import './Form.scss';
import {useState, Children, cloneElement} from 'react';
import {FormProps} from './FormProps';
import {AlignProps} from '../../props/AlignProps';
import {getClassName} from '../../helper';
import FormSection from './FormSection';
import {DirectionProps} from '../../props/DirectionProps';

interface FormInstanceProps extends FormProps, AlignProps, DirectionProps {
  action?: (
    fields: Map<string, {value: string, error: string}>,
    setSubmitMessage: (message: string, color: string) => void
  ) => void
  undecorated?: boolean
}

const Form = (props: FormInstanceProps) => {
  const [fields] = useState(new Map<string, {value: string, error: string}>());
  const [message, setMessage] = useState('');
  const [messageColor, setMessageColor] = useState('');

  const setSubmitMessage = (message: string, color: string) => {
    setMessage(message);
    setMessageColor(color);
  }

  const getFieldsError = () => {
    let error = '';
    fields.forEach(field => {
      if (!error && field.error) error = field.error;
    });
    return error;
  };

  const submit = () => {
    const fieldsError = getFieldsError();
    setSubmitMessage(fieldsError, 'red');
    !fieldsError && props.action && props.action(fields, setSubmitMessage);
  };

  const onFormUpdated = () => {
    setMessage('');
  };

  const className = getClassName('form', props, ['undecorated']);

  return (
    <form className={className} onChange={onFormUpdated}>
      <FormSection 
        direction={props.direction}
        fields={fields} 
        submit={submit} 
        submitMessage={message}
        submitMessageColor={messageColor}
        setSubmitMessage={setSubmitMessage}
        align={props.align}
      >
        {Children.map(props.children, child =>
          child ?
            cloneElement(child, {
              fields: fields,
              submit: submit,
              submitMessage: message,
              submitMessageColor: messageColor,
              setSubmitMessage: setSubmitMessage,
            }) :
            child
        )}
      </FormSection>
    </form>
  );
}

export default Form;

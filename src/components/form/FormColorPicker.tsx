import './FormColorPicker.scss';
import React, {useEffect, useState} from 'react';
import {FormProps} from './FormProps';

interface FormColorProps extends FormProps {
  initialValue: string
  disabled?: boolean
}

const FormColorPicker = (props: FormColorProps) => {  
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(props.initialValue);
  }, [props.initialValue]);

  useEffect(() => {
    if (props.name) {
      props.fields?.set(props.name, {value: value, error: ''});
    }
  }, [props, value]);

  const onChangeColor = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  return (
    <input 
      className='form-color-picker'
      type='color'
      value={value}
      onChange={onChangeColor}
      disabled={props.disabled}
    />
  );
}

export default FormColorPicker;

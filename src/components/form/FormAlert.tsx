import './FormAlert.scss';
import {FormProps} from './FormProps';
import notice from '../../assets/notice.svg';
import close from '../../assets/close.svg';
import Icon from '../Icon';
import Text from '../Text';

const FormAlert = (props: FormProps) => {
  const color = props.submitMessageColor;

  const onClose = () => {
    props.setSubmitMessage && props.setSubmitMessage('', 'red');
  };

  return (
    <div className={`form-alert color-${color}`}>
      {props.submitMessage ?
        <div className='alert-notification'>
          <Icon src={notice} size='small' color={color}/>
          <Text size='small' color={color}>
            {props.submitMessage}
          </Text>
        </div> : ''}
      {props.submitMessage ?
        <div className='alert-close'>
          <Icon src={close} size='small' color={color} onClick={onClose}/>
        </div> : ''}
    </div>
  );
}

export default FormAlert;

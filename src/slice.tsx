import {createSlice, PayloadAction} from '@reduxjs/toolkit';

const initialState: {
  username: string,
  enemyUsername: string,
  roles: string[],
  theme: string,
  snakeColor: string,
  enemySnakeColor: string
} = {
  username: '',
  enemyUsername: '',
  roles: [],
  theme: 'dark',
  snakeColor: '#00ffff',
  enemySnakeColor: '#ffffff'
};

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setUsername: (state, action: PayloadAction<string>) => {
      state.username = action.payload;
    },
    setEnemyUsername: (state, action: PayloadAction<string>) => {
      state.enemyUsername = action.payload;
    },
    setRoles: (state, action: PayloadAction<string[]>) => {
      state.roles = action.payload;
    },
    setTheme: (state, action: PayloadAction<string>) => {
      state.theme = action.payload;
    },
    setSnakeColor: (state, action: PayloadAction<string>) => {
      state.snakeColor = action.payload;
    },
    setEnemySnakeColor: (state, action: PayloadAction<string>) => {
      state.enemySnakeColor = action.payload;
    }
  },
});

export const {
  setUsername,
  setEnemyUsername,
  setRoles,
  setTheme,
  setSnakeColor,
  setEnemySnakeColor
} = appSlice.actions;

export default appSlice.reducer;

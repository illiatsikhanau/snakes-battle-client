const getClassName = (prefix: string, props: object, keys: string[]): string => {
  const className = keys.map(key => {
    const value = props[key as keyof typeof props];
    if (value === true) {
      return key;
    } else if (value) {
      return `${key}-${value}`;
    } else {
      return '';
    }
  }).filter(name => name).join(' ');

  return `${prefix} ${className}`.trim();
}

export {getClassName};

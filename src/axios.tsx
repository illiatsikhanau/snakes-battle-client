import axios from 'axios';
import {getCookie} from 'typescript-cookie';

axios.interceptors.request.use(function (config) {
  config.baseURL = process.env.REACT_APP_API_ADDRESS;
  config.headers.Authorization = `Basic ${getCookie('token')}`;
  return config;
});

export default axios;

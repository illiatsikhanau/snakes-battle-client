import './index.scss';
import {useEffect, useState} from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter, Routes, Route, Navigate, useLocation} from 'react-router-dom';
import {Provider} from 'react-redux';
import {useAppDispatch, useAppSelector} from './hooks';
import {store} from './store';
import {setRoles, setSnakeColor, setTheme, setUsername} from './slice';
import {setSocket} from './socket';
import axios from './axios';
import {getCookie} from 'typescript-cookie';
import Header from './sections/Header';
import Content from './sections/Content';
import Footer from './sections/Footer';
import Banner from './sections/Banner';
import Login from './sections/Login';
import Account from './sections/Account';
import Spinner from './sections/Spinner';
import Games from './sections/Games';
import Game from './sections/Game';

const App = () => {
  const location = useLocation();
  const username = useAppSelector((state) => state.app.username);

  return (
    <>
      <Header/>
      <Content>
        <Routes>
          <Route index element={username ? <Navigate to='/games'/> : <Banner/>}/>
          <Route path='/signup' element={<Login/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/account' element={username ? <Account/> : <Navigate to='/login'/>}/>
          <Route path='/games' element={username ? <Games/> : <Navigate to='/login'/>}/>
          <Route path='/game' element={username ? <Game/> : <Navigate to='/login'/>}/>
          <Route path='*' element={<Navigate to='/'/>}/>
        </Routes>
      </Content>
      {location.pathname === '/game' ? '' : <Footer/>}
    </>
  );
};

const Loader = () => {
  const dispatch = useAppDispatch();
  
  const theme = useAppSelector((state) => state.app.theme);

  const [loading, setLoading] = useState(true);

  const getInfo = async () => {
    try {
      const info = await axios.get('/my/info');
      if (info.data.username) {
        dispatch(setTheme(info.data.theme));
        dispatch(setUsername(info.data.username));
        dispatch(setRoles(info.data.roles));
        dispatch(setSnakeColor(info.data.snake_color));
        setSocket(new WebSocket(`${process.env.REACT_APP_WS_ADDRESS}?token=${getCookie('token')}`));
      }
    } catch {} finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getInfo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    loading ? 
      <Spinner/> : 
      <div className={`app theme-${theme}`}>
        <BrowserRouter>
          <App/>
        </BrowserRouter>
      </div>
  );
};

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <Loader/>
  </Provider>
);
